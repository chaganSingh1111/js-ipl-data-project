const csvtojson = require('csvtojson')

const csvFilePath = '/home/hi/Desktop/ipl-project/src/data/matches.csv'

const fs = require('fs')

csvtojson()
.fromFile(csvFilePath)
.then((data)=>{
    

    const matcheObj = data.reduce((acc,match)=>{

        if(acc[match.season] === undefined){
            acc[match.season] = 1
        }
        else{
            acc[match.season] = acc[match.season]+1
        }
        return acc
    },{})
    
    console.log(matcheObj)

    fs.writeFileSync('/home/hi/Desktop/ipl-project/src/public/output/1-matches-per-year.json',JSON.stringify(matcheObj))
})


