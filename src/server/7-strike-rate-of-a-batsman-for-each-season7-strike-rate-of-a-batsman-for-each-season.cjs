// const csvDeliveriesFilePath='/home/hi/Desktop/ipl-project/src/data/deliveries.csv'
// const csvMatchesFilePath = '/home/hi/Desktop/ipl-project/src/data/matches.csv'
// const fs = require('fs')
// const csvtojson=require('csvtojson')


// let matchesPerYear = {};
// csvtojson()
// .fromFile(csvMatchesFilePath)
// .then((jsonObj)=>{ 
//     const matchesPerYear = jsonObj.reduce((acc,jsonObjIdx)=>{
//         let year = jsonObjIdx.season;
//         if(matchesPerYear[jsonObj[idx].season] == undefined){
//             acc[jsonObj[idx].season] = []
//         }
//         matchesPerYear[year].push(jsonObjIdx.id);
//         return acc
//     },{})

//     console.log(matchesPerYear)

//     // for(let idx in jsonObj){
//     //     let year = jsonObj[idx].season;
//     //     if(matchesPerYear[jsonObj[idx].season] == undefined){
//     //         matchesPerYear[jsonObj[idx].season] = [];
//     //     }
//     //     matchesPerYear[year].push(jsonObj[idx].id);
//     // }

//     csvtojson()
//     .fromFile(csvDeliveriesFilePath)
//     .then((jsonObj)=>{
//         let batsmen = []; 
//         for(let idx1 in jsonObj){
//             let matchId = jsonObj[idx1].match_id;
//             let year = findYear(matchId);
//             let found = false;
//             for(idx2 in batsmen){
//                 if(batsmen[idx2].year == year && batsmen[idx2].name == jsonObj[idx1].batsman){
//                     batsmen[idx2].totalRuns = parseInt(batsmen[idx2].totalRuns)+ parseInt(jsonObj[idx1].batsman_runs);
//                     if(jsonObj[idx1].wide_runs == '0' && jsonObj[idx1].noball_runs == '0'){
//                         batsmen[idx2].totalBalls += 1;
//                     }
//                     found = true;
//                     break;
//                 }
//             }
//             if(!found){
//                 let ballsBowled  = 0;
//                 if(jsonObj[idx1].wide_runs == '0' && jsonObj[idx1].noball_runs == '0'){
//                     ballsBowled = 1;
//                 }
//                 batsmen.push(new batsman(year,jsonObj[idx1].batsman, jsonObj[idx1].batsman_runs, ballsBowled));
//             }
//         }
//         calculateStrikeRate(batsmen);
//         console.log(batsmen);
//         fs.writeFileSync('/home/hi/Desktop/ipl-project/src/public/output/7-strike-rate-of-a-batsman-for-each-season.json',JSON.stringify(batsmen)); 
//     })

// })

// function findYear(matchId){
//     for(let idx3 in matchesPerYear){
//         for(let idx4 in matchesPerYear[idx3]){
//             if(matchId==matchesPerYear[idx3][idx4])
//             return idx3;
//         }
        
//     }
// }

// function calculateStrikeRate(batsmen){
//     for(idx5 in batsmen){
//         batsmen[idx5].strikeRate = ((batsmen[idx5].totalRuns/batsmen[idx5].totalBalls)*100).toFixed(2);
//     }
// }

// function batsman(year,name, totalRuns, totalBalls){
//     this.year = year;
//     this.name = name;
//     this.totalRuns= totalRuns;
//     this.totalBalls= totalBalls;
//     this.strikeRate = 0;
// }


const csvToJson = require("csvtojson");
const matchData = "src/data/matches.csv";
const deliveriesData = "src/data/deliveries.csv";
const fs = require("fs");


csvToJson()
    .fromFile(matchData)
    .then((data) => {
      const matchesPerYear = data.reduce((acc, item) => {
            acc.push(item.season);
            return acc;
      }, []);

      csvToJson()
        .fromFile(deliveriesData)
        .then((data) => {
          let batsmen = data.reduce((acc, item) => {
            let boolean = false;

            const year = matchesPerYear[item.match_id];

            let arr1 = acc.find((ipldata) => {
              return ipldata.batsman1 === item.batsman;
            });

            if (arr1) {
              boolean = true;

              arr1.totalRun += Number(item.batsman_runs);
              arr1.bowl += 1;
              arr1.strikRate = (arr1.totalRun * 100) / arr1.bowl;
            }

            if (!boolean) {
              let initalStrick = (item.batsman_runs * 100) / 1;

              let obj = {
                per_year: year,
                batsman1: item.batsman,
                totalRun: Number(item.batsman_runs),
                bowl: 1,
                strikRate: initalStrick,
              };
              acc.push(obj);
            }

            return acc;
          }, []);
          console.log(batsmen)

          fs.writeFileSync('/home/hi/Desktop/ipl-project/src/public/output/7-strike-rate-of-a-batsman-for-each-season.json',JSON.stringify(batsmen));
        });
    });
