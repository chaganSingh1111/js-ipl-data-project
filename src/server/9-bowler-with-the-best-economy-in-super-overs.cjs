const csvDeliveriesFilePath ="/home/hi/Desktop/ipl-project/src/data/deliveries.csv";
const fs = require("fs");
const csvtojson = require("csvtojson");

csvtojson()
    .fromFile(csvDeliveriesFilePath)
    .then((data) => {
      const superOverBowler = data.reduce((acc, items) => {
        let boolean = false;
        if (items.is_super_over != 0) {
          acc.filter((bowl) => {
            if (bowl.bowlerName == items.bowler) {
              bowl.totalRun += Number(items.is_super_over);
              bowl.totalBowl += 1;
              bowl.economy_superOver = (bowl.totalRun * 6) / bowl.totalBowl;
              boolean = true;
            }
          });

          if (!boolean) {
            let initalEconomic = (items.is_super_over * 6) / 1;
            let obj = {
              bowlerName: items.bowler,
              totalRun: 1,
              totalBowl: 1,
              economy_superOver: initalEconomic,
            };
            acc.push(obj);
          }
        }
        return acc;
      }, []);

      superOverBowler.sort((bowler1, bowler2) => {
        if (bowler1.economy_superOver > bowler2.economy_superOver) {
          return 1;
        }
        if (bowler1.economy_superOver < bowler2.economy_superOver) {
          return -1;
        } else {
          return 0;
        }
      });

      console.log(superOverBowler[0]);
      fs.writeFileSync('/home/hi/Desktop/js-ipl-data-project/src/server/9-bowler-with-the-best-economy-in-super-overs.cjs',JSON.stringify(superOverBowler[0]))

    });

