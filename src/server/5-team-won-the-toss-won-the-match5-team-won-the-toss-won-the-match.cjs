const csvMatchesFilePath = '/home/hi/Desktop/ipl-project/src/data/matches.csv'
const fs = require('fs')
const csvtojson=require('csvtojson')

csvtojson()
.fromFile(csvMatchesFilePath)
.then((data)=>{

    const teams = data.reduce((acc,dataIdx)=>{
        if(dataIdx.toss_winner == dataIdx.winner){
            acc.push(dataIdx.winner)
        }
        return acc
    },[])

    const tossAndWinPerTeam = teams.reduce((acc,team) => {
        if(acc[team]===undefined){
            acc[team]=1
        }
        else{
            acc[team] +=1
        }
          return acc 

        },{})
    console.log(tossAndWinPerTeam)
    
    fs.writeFileSync('/home/hi/Desktop/ipl-project/src/public/output/5-team-won-the-toss-won-the-match.json',JSON.stringify(tossAndWinPerTeam))
})
