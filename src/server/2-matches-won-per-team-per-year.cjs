const csvtojson = require('csvtojson')

const csvFilePath = '/home/hi/Desktop/ipl-project/src/data/matches.csv'

const fs = require('fs')

csvtojson()
.fromFile(csvFilePath)
.then((data)=>{
    

const matcheWonPerYear = data.reduce((acc,match)=>{
    
    let winner  = match.winner
    let year = match.season
    let boolean = false


     const obj = acc.filter((accIdx)=>{
        return (accIdx.year == year && accIdx.team == winner)
                        
    })
    if(obj.length){
        boolean=true
        obj[0].totalwin+=1
    }
    
    if(!boolean){
        let matchWonObj = {
            year : match.season,
            team : match.winner,
            totalwin : 1
        }
        acc.push(matchWonObj)
    }
    return acc 

   },[])

        
    console.log(matcheWonPerYear)

    fs.writeFileSync('/home/hi/Desktop/ipl-project/src/public/output/2-matches-won-per-team-per-year.json',JSON.stringify(matcheWonPerYear,null,2),"utf-8")
})
