// const csvDeliveriesFilePath='/home/hi/Desktop/ipl-project/src/data/deliveries.csv'
// const csvMatchesFilePath = '/home/hi/Desktop/ipl-project/src/data/matches.csv'
// const csvtojson=require('csvtojson')
// const fs = require('fs')


// //let matchIds = []

// csvtojson()
// .fromFile(csvMatchesFilePath)
// .then((jsonObj)=>{

//     const matcheIds2 = jsonObj.reduce((acc,jsonObjIdx)=>{
//         if(jsonObjIdx.season === "2015"){
//             acc.push(jsonObjIdx.id);
//         }
//         return acc
//     },[])

//     csvtojson()
//     .fromFile(csvDeliveriesFilePath)
//     .then((jsonObj)=>{
//         let bowlers =[];
//         for(let i in jsonObj){
//             if(matcheIds2.includes(jsonObj[i].match_id)){
//                 let found = false;
//                 for(let j in bowlers){
//                     if(bowlers[j].name == jsonObj[i].bowler){
//                         if(jsonObj[i].wide_runs == '0' && jsonObj[i].noball_runs == '0'){
//                             bowlers[j].ballsBowled++;
//                         }
//                         bowlers[j].runsConceded = parseInt(bowlers[j].runsConceded) + parseInt(jsonObj[i].total_runs) - parseInt(jsonObj[i].legbye_runs) -parseInt(jsonObj[i].bye_runs) -parseInt(jsonObj[i].penalty_runs);
//                         found = true
//                     }
//                 }
//                 if(!found){    
//                     let runsConceded = parseInt(jsonObj[i].total_runs) - parseInt(jsonObj[i].legbye_runs) -parseInt(jsonObj[i].bye_runs) -parseInt(jsonObj[i].penalty_runs)
//                     let ballsBowled = 0
//                     if(jsonObj[i].wide_runs == '0' && jsonObj[i].noball_runs == '0'){
//                         ballsBowled++
//                     }
//                     bowlers.push(new bowlerStats(jsonObj[i].bowler, runsConceded , ballsBowled))

//                 }
//             }
//         }
//         for(let i in bowlers){
//             bowlers[i].economy = parseFloat(bowlers[i].runsConceded/(bowlers[i].ballsBowled/6)).toFixed(2)
//         }
//         bowlers.sort(compare)
//         let top10Bowlers = bowlers.slice(0,10)
//         console.log(top10Bowlers)



//     fs.writeFileSync('/home/hi/Desktop/ipl-project/src/public/output/4-top-10-economical-bowlers.json',JSON.stringify(top10Bowlers))
// })

// })

// function compare( a, b ) {
//     if ( parseFloat(a.economy) < parseFloat(b.economy) ){
//       return -1
//     }
//     if ( parseFloat(a.economy) > parseFloat(b.economy) ){
//       return 1
//     }
//     return 0
//   }
  

// function bowlerStats(name, runsConceded, ballsBowled){
//     this.name = name
//     this.runsConceded = runsConceded
//     this.ballsBowled = ballsBowled
//     this.economy = 0
// }


const csvToJson = require("csvtojson");
const matchData = "src/data/matches.csv";
const deliveriesData = "src/data/deliveries.csv";
const fs = require("fs");


csvToJson()
    .fromFile(matchData)
    .then((data) => {
        const matcheIds2 = data.reduce((acc,dataIdx)=>{
                    if(dataIdx.season === "2015"){
                       acc.push(dataIdx.id);
                    }
                    return acc
                },[])

      csvToJson()
        .fromFile(deliveriesData)
        .then((data) => {
          const economyOfBowlers = data.reduce((acc, item) => {
            if (matcheIds2.includes(item.match_id)) {
              if (acc[item.bowler] === undefined) {
                acc[item.bowler] = {
                  run: Number(item.total_runs),
                  ball: 1,
                  economy: 6,
                };
              } else {
                acc[item.bowler].run += Number(item.total_runs);
                acc[item.bowler].ball += 1;
                acc[item.bowler].economy =
                  (acc[item.bowler].run * 6) / acc[item.bowler].ball;
              }
            }
            return acc;
          }, {});

          const keys = Object.keys(economyOfBowlers);

          keys.sort(compare);
          const top10Bowlers = keys.slice(0, 10);
          console.log(top10Bowlers)

          fs.writeFileSync('/home/hi/Desktop/ipl-project/src/public/output/4-top-10-economical-bowlers.json',JSON.stringify(top10Bowlers))
        });
    });



    function compare( a, b ) {
            if ( parseFloat(a.economy) < parseFloat(b.economy) ){
              return -1
            }
            if ( parseFloat(a.economy) > parseFloat(b.economy) ){
              return 1
            }
            return 0
          }