const csvMatchesFilePath = "/home/hi/Desktop/ipl-project/src/data/matches.csv";
const fs = require("fs");

const csvtojson = require("csvtojson");

csvtojson()
  .fromFile(csvMatchesFilePath)
  .then((jsonObj) => {
    const players = jsonObj.reduce((acc, jsonObjIdx) => {
      let boolean = false;

      const arr = acc.filter((accIdx) => {
        return (
          jsonObjIdx.player_of_match == accIdx.name &&
          jsonObjIdx.season == accIdx.year
        );
      });
      if (arr.length) {
        boolean = true;
        arr[0].potm += 1;
      }

      if (!boolean) {
        let matchWonObj = {
          year: jsonObjIdx.season,
          name: jsonObjIdx.player_of_match,
          potm: 1,
        };
        acc.push(matchWonObj);
      }

      return acc;
    }, []);
    players.sort(compare);


    const playerOfTheYear = players.reduce((acc, data) => {
      if (acc[data.year] === undefined) {
        acc[data.year] = {};
        acc[data.year][data.name] = data.potm;
      }
      return acc;
    }, {});

    console.log(playerOfTheYear)
    fs.writeFileSync("/home/hi/Desktop/ipl-project/src/public/output/6-player-with-the-highest-man-of-match.json",JSON.stringify(playerOfTheYear));
  })

function compare(a, b) {
  if (parseInt(a.year) < parseInt(b.year)) {
    return 1;
  } else if (parseInt(a.year) > parseInt(b.year)) {
    return -1;
  } else if (parseInt(a.potm) < parseInt(b.potm)) {
    return 1;
  } else if (parseInt(a.potm) > parseInt(b.potm)) {
    return -1;
  }
}
