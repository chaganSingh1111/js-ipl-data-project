const csvDeliveriesFilePath='/home/hi/Desktop/ipl-project/src/data/deliveries.csv'
const csvMatchesFilePath = '/home/hi/Desktop/ipl-project/src/data/maches.csv'

const fs = require('fs')
const csvtojson=require('csvtojson')
csvtojson()
.fromFile(csvDeliveriesFilePath)
.then((jsonObj)=>{

    const wickets2 = jsonObj.reduce((acc,jsonObjIdx)=>{
        if(jsonObjIdx.player_dismissed != ''){
            let boolean = false

    
         const arr = acc.filter((accIdx)=>{
            return (accIdx.bowler==jsonObjIdx.bowler && accIdx.dissmisedPlayer==jsonObjIdx.player_dismissed)
                            
        })
        if(arr.length){
            boolean=true
            arr[0].times+=1
        }
        
        if(!boolean){
            let matchWonObj = {
                bowler : jsonObjIdx.bowler,
                dissmisedPlayer : jsonObjIdx.player_dismissed,
                times : 1
            }
            acc.push(matchWonObj)
        }
        }
        return acc 
    
       },[])

    wickets2.sort(compare)
    console.log(wickets2)
    fs.writeFileSync('/home/hi/Desktop/ipl-project/src/public/output/8-highest-number-of-times-one-player-dismissed.json',JSON.stringify(wickets2[0]))
})

function compare( a, b){
    if(a.times > b.times){
        return -1;
    }
    else if(a.times < b.times){
        return 1
    }
}
